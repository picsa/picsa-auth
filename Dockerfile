FROM praekeltfoundation/django-bootstrap
ENV DJANGO_SETTINGS_MODULE "picsa_auth.settings"
RUN ./manage.py collectstatic --noinput
ENV APP_MODULE "picsa_auth.wsgi:application"
