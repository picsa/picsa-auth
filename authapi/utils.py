from authapi.models import PicsaPermission


def get_user_permissions(user):
    '''Returns the queryset of permissions for the given user.'''
    permissions = PicsaPermission.objects.all()
    # User must be on a team that grants the permission
    permissions = permissions.filter(picsateam__users=user)
    # The team must be active
    permissions = permissions.filter(picsateam__archived=False)
    # The organization of that team must be active
    permissions = permissions.filter(
        picsateam__organization__archived=False)
    return permissions


def find_permission(
        permissions, permission_type, object_id=None, namespace=None):
    '''Given a queryset of permissions, filters depending on the permission
    type, and optionally an object id and namespace.'''
    if object_id is not None:
        return permissions.filter(
            type=permission_type, object_id=object_id, namespace=namespace)
    return permissions.filter(type=permission_type)
