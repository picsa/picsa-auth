from django.contrib.auth.models import User
from django.db import models


class PicsaOrganization(models.Model):
    title = models.TextField()
    users = models.ManyToManyField(User)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    archived = models.BooleanField(default=False)

    def get_active_teams(self):
        return self.picsateam_set.filter(archived=False)

    def get_active_users(self):
        return self.users.filter(is_active=True)


class PicsaPermission(models.Model):
    type = models.TextField()
    object_id = models.TextField(null=True)
    namespace = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class PicsaTeam(models.Model):
    title = models.TextField()
    organization = models.ForeignKey(PicsaOrganization)
    users = models.ManyToManyField(User)
    permissions = models.ManyToManyField(PicsaPermission)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    archived = models.BooleanField(default=False)

    def get_active_users(self):
        return self.users.filter(is_active=True)
