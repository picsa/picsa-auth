from django.conf.urls import include, url
from rest_framework_extensions import routers

from authapi import views

router = routers.ExtendedSimpleRouter()

org_router = router.register(r'organizations', views.OrganizationViewSet)
team_router = router.register(r'teams', views.TeamViewSet)
router.register(r'users', views.UserViewSet)

org_router.register(
    r'users', views.OrganizationUsersViewSet,
    base_name='picsaorganization-users',
    parents_query_lookups=['organization'])
orgteam_router = org_router.register(
    r'teams', views.OrganizationTeamViewSet,
    base_name='picsaorganization-teams',
    parents_query_lookups=['organization'])

orgteam_router.register(
    r'permissions', views.TeamPermissionViewSet,
    base_name='picsaorganization-teams-permissions',
    parents_query_lookups=['picsateam__organization', 'picsateam'])
orgteam_router.register(
    r'users', views.TeamUsersViewSet,
    base_name='picsaorganization-teams-users',
    parents_query_lookups=['picsateam__organization', 'picsateam'])


team_router.register(
    r'permissions', views.TeamPermissionViewSet,
    base_name='picsateam-permissions',
    parents_query_lookups=['picsateam'])
team_router.register(
    r'users', views.TeamUsersViewSet,
    base_name='picsateam-users',
    parents_query_lookups=['picsateam'])

urlpatterns = [
    url(r'^', include(router.urls)),
    url(
        r'^user/$', views.UserPermissionsView.as_view(),
        name='get-user-permissions'),
    url(r'^user/tokens/$', views.TokenView.as_view(), name='create-token'),
]
