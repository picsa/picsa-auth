# picsa-auth
Picsa Auth Service. User and permissions store, authentication and authorization.

Forked from the awesome work of praekelt/seed-auth-api and built upon for
Picsa specific needs

## Running

 * `pip install -e .`
 * `psql -U postgres -c "CREATE DATABASE picsa_auth;"`
 * `./manage.py migrate`
 * `./manage.py createsuperuser`
 * `./manage.py runserver --settings=picsa_auth.testsettings`

## Running tests

 * `pip install -e .`
 * `pip install -r requirements-dev.txt`
 * `py.test --ds=picsa_auth.testsettings authapi
